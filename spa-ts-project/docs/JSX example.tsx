

// JSX:
var html = <div id="123">placki <input/> { 1 + 2 } </div>;

// compiled JSX:
var html = h('div',{id:123},'placki', h('input'), 1 + 2)