import axios, { AxiosError } from "axios";
import { AuthService } from "./AuthService";
import { AlbumsResponse, Album } from "../models/Album";
import { authService } from "../services";

axios.interceptors.request.use(request => {
  request.headers["Authorization"] = `Bearer ${authService.getToken()}`;
  return request;
});
axios.interceptors.response.use(
  resp => resp,
  error => {
    if (error.response && error.response.status == 401) {
      // throw new Error("Bad token");
      authService.authorize();
    }
    if (error.response && error.response.status == 400) {
      throw new Error("Bad request");
    }
  }
);

export class MusicSearchService {
  constructor(private auth: AuthService, private search_url: string) {}

  search(query = "batman") {
    if (query == "") {
      return Promise.reject(new Error("Please enter query"));
    }

    return axios
      .get<AlbumsResponse>(this.search_url, {
        params: {
          type: "album",
          q: query
        }
        // https://github.com/axios/axios#cancellation
        // cancelToken: new axios.CancelToken(()=>{})
      })
      .then(resp => {
        return resp.data.albums.items;
      });
  }
}

// .catch((error: AxiosError) => {
//   // if (error.response && error.response.status == 401) {
//   //   // throw new Error("Bad token");
//   //   this.auth.authorize();
//   // }
//   // if (error.response && error.response.status == 400) {
//   //   throw new Error("Bad request");
//   // }
//   return [] as Album[];
// });

// var dev = new Promise((resolve)=>{
//   resolve(...)
// })

// dev.then(...)
// dev.then(...)
// dev.then(...)

/* 
function echo(msg, fail = false ){
	return new Promise((resolve,reject)=>{
		setTimeout(() => { 
			fail? reject(msg) : resolve(msg);
        }, 2000)
    })
}

var m = echo('lubie')

m.then(r => r + ' placki')
 .then(console.log)


m
.then(r => echo(r + ' ciastka',true)  )
.catch(r => 'nie lubie ciastek ')
.then(r2 => echo( r2 + ' i lody' ) )
.then(console.log)

*/
