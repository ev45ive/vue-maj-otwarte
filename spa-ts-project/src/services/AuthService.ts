// https://github.com/dgrubelic/vue-authenticate
export class AuthService {
  constructor(
    private auth_url: string,
    private client_id: string,
    private redirect_uri: string,
    private response_type = "token"
  ) {
    const JSONtoken = sessionStorage.getItem("token");
    if (JSONtoken) {
      this.token = JSON.parse(JSONtoken);
    }

    if (location.hash) {
      const match = location.hash.match(/access_token=([^&]*)/);
      if (match && match[1]) {
        this.token = match[1];
        location.hash = "";
        sessionStorage.setItem("token", JSON.stringify(this.token));
      }
    }
  }

  authorize() {
    sessionStorage.removeItem("token");
    //
    const url =
      `${this.auth_url}?` +
      `client_id=${this.client_id}` +
      `&redirect_uri=${this.redirect_uri}` +
      `&response_type=${this.response_type}`;

    location.href = url;
  }

  token: string | null = null;

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
