import Vue from "vue";
import Vuex from "vuex";
import { CounterModule } from "./modules/CounterModule";
import PlaylistsModule from "./modules/PlaylistsModule";
import SearchModule from "./modules/SearchModule";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    counter: CounterModule,
    playlists: PlaylistsModule,
    search: SearchModule
  }
});
