import { AuthService } from "./services/AuthService";
import { MusicSearchService } from "./services/MusicSearchService";

export const authService = new AuthService(
  "https://accounts.spotify.com/authorize",
  "70599ee5812a4a16abd861625a38f5a6",
  "http://localhost:8080/"
);
/*
  https://developer.spotify.com/dashboard/
  placki@placki.com
  ******
*/

export const musicSearch = new MusicSearchService(
  authService,
  "https://api.spotify.com/v1/search"
);


// Vue-Resource
// <button @click="$http.get('...')
// .then(r => results = r.data)">
//    Refesh
// </button