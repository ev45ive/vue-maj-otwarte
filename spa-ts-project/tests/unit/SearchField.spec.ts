import { shallowMount, mount, Wrapper } from "@vue/test-utils";
import SearchField from "@/components/SearchField.vue";

describe("SearchField.vue", () => {
  let query: string, wrapper: Wrapper<SearchField>;

  beforeEach(() => {
    query = "new query";
    wrapper = shallowMount(SearchField, {
      propsData: { query }
    });
  });

  it("renders input with query from props", () => {
    // expect(wrapper.html()).toMatch(query);
    const elem = wrapper.find("input").element;
    const value = (elem as HTMLInputElement).value;
    expect(value).toBe(query);
  });

  // https://vue-test-utils.vuejs.org/guides/#testing-key-mouse-and-other-dom-events
  it("should search on keyup enter", () => {
    wrapper.find("input").trigger("keyup", {
      keyCode: 13,
      code: "Enter",
      key: "Enter"
    });
    expect(wrapper.emitted("search")).toEqual([[query]]);
  });

  it("auto search after 400ms", () => {
    jest.useFakeTimers();
    const input = wrapper.find("input");
    input.trigger("input");
    expect(wrapper.emitted("search")).not.toBeDefined();

    /* Debounce - no search */
    jest.advanceTimersByTime(300);
    input.trigger("input");
    jest.advanceTimersByTime(100);
    expect(wrapper.emitted("search")).not.toBeDefined();

    /* Search */
    jest.advanceTimersByTime(400);
    expect(wrapper.emitted("search")).toEqual([[query]]);

    jest.useRealTimers();
  });
});
